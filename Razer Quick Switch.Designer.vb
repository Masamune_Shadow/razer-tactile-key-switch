﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRazerQuickSwitch
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtNewOrder = New System.Windows.Forms.TextBox()
        Me.txtCurrentOrder = New System.Windows.Forms.TextBox()
        Me.cmdMoveDown = New System.Windows.Forms.Button()
        Me.cmdMoveUp = New System.Windows.Forms.Button()
        Me.cmdMoveFirst = New System.Windows.Forms.Button()
        Me.cmdMoveLast = New System.Windows.Forms.Button()
        Me.cmdOk = New System.Windows.Forms.Button()
        Me.cmdOrderApply = New System.Windows.Forms.Button()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.lstInput = New System.Windows.Forms.ListBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.cmdOpenFileDialog = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rdoSWTOR = New System.Windows.Forms.RadioButton()
        Me.rdoDeathstalker = New System.Windows.Forms.RadioButton()
        Me.rdoBlade = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmdGetAppList = New System.Windows.Forms.Button()
        Me.cmdApply = New System.Windows.Forms.Button()
        Me.cmdAbout = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtNewOrder
        '
        Me.txtNewOrder.Enabled = False
        Me.txtNewOrder.Location = New System.Drawing.Point(661, 161)
        Me.txtNewOrder.Name = "txtNewOrder"
        Me.txtNewOrder.Size = New System.Drawing.Size(100, 20)
        Me.txtNewOrder.TabIndex = 25
        Me.txtNewOrder.TabStop = False
        Me.txtNewOrder.Visible = False
        '
        'txtCurrentOrder
        '
        Me.txtCurrentOrder.Location = New System.Drawing.Point(661, 115)
        Me.txtCurrentOrder.Name = "txtCurrentOrder"
        Me.txtCurrentOrder.Size = New System.Drawing.Size(100, 20)
        Me.txtCurrentOrder.TabIndex = 24
        '
        'cmdMoveDown
        '
        Me.cmdMoveDown.Location = New System.Drawing.Point(358, 281)
        Me.cmdMoveDown.Name = "cmdMoveDown"
        Me.cmdMoveDown.Size = New System.Drawing.Size(131, 41)
        Me.cmdMoveDown.TabIndex = 23
        Me.cmdMoveDown.Text = "Move Selected &Down"
        Me.cmdMoveDown.UseVisualStyleBackColor = True
        '
        'cmdMoveUp
        '
        Me.cmdMoveUp.Location = New System.Drawing.Point(358, 220)
        Me.cmdMoveUp.Name = "cmdMoveUp"
        Me.cmdMoveUp.Size = New System.Drawing.Size(131, 41)
        Me.cmdMoveUp.TabIndex = 22
        Me.cmdMoveUp.Text = "Move Selected &Up"
        Me.cmdMoveUp.UseVisualStyleBackColor = True
        '
        'cmdMoveFirst
        '
        Me.cmdMoveFirst.Enabled = False
        Me.cmdMoveFirst.Location = New System.Drawing.Point(687, 220)
        Me.cmdMoveFirst.Name = "cmdMoveFirst"
        Me.cmdMoveFirst.Size = New System.Drawing.Size(113, 23)
        Me.cmdMoveFirst.TabIndex = 21
        Me.cmdMoveFirst.TabStop = False
        Me.cmdMoveFirst.Text = "Move Selected &First"
        Me.cmdMoveFirst.UseVisualStyleBackColor = True
        Me.cmdMoveFirst.Visible = False
        '
        'cmdMoveLast
        '
        Me.cmdMoveLast.Enabled = False
        Me.cmdMoveLast.Location = New System.Drawing.Point(687, 284)
        Me.cmdMoveLast.Name = "cmdMoveLast"
        Me.cmdMoveLast.Size = New System.Drawing.Size(141, 23)
        Me.cmdMoveLast.TabIndex = 20
        Me.cmdMoveLast.TabStop = False
        Me.cmdMoveLast.Text = "Move Selected &Last"
        Me.cmdMoveLast.UseVisualStyleBackColor = True
        Me.cmdMoveLast.Visible = False
        '
        'cmdOk
        '
        Me.cmdOk.Location = New System.Drawing.Point(577, 333)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.Size = New System.Drawing.Size(75, 23)
        Me.cmdOk.TabIndex = 19
        Me.cmdOk.Text = "&OK"
        Me.cmdOk.UseVisualStyleBackColor = True
        '
        'cmdOrderApply
        '
        Me.cmdOrderApply.Enabled = False
        Me.cmdOrderApply.Location = New System.Drawing.Point(782, 158)
        Me.cmdOrderApply.Name = "cmdOrderApply"
        Me.cmdOrderApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdOrderApply.TabIndex = 18
        Me.cmdOrderApply.TabStop = False
        Me.cmdOrderApply.Text = "&Apply"
        Me.cmdOrderApply.UseVisualStyleBackColor = True
        Me.cmdOrderApply.Visible = False
        '
        'cmdCancel
        '
        Me.cmdCancel.Location = New System.Drawing.Point(686, 333)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 17
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Enabled = False
        Me.label3.Location = New System.Drawing.Point(525, 164)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(88, 13)
        Me.label3.TabIndex = 16
        Me.label3.Text = "New Item's Order"
        Me.label3.Visible = False
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(525, 115)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(100, 13)
        Me.label2.TabIndex = 15
        Me.label2.Text = "Current Item's Order"
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(12, 55)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(87, 13)
        Me.label1.TabIndex = 14
        Me.label1.Text = "Current List items"
        '
        'lstInput
        '
        Me.lstInput.AllowDrop = True
        Me.lstInput.FormattingEnabled = True
        Me.lstInput.Items.AddRange(New Object() {""})
        Me.lstInput.Location = New System.Drawing.Point(12, 71)
        Me.lstInput.Name = "lstInput"
        Me.lstInput.ScrollAlwaysVisible = True
        Me.lstInput.Size = New System.Drawing.Size(301, 277)
        Me.lstInput.TabIndex = 13
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'cmdOpenFileDialog
        '
        Me.cmdOpenFileDialog.AutoSize = True
        Me.cmdOpenFileDialog.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.cmdOpenFileDialog.Enabled = False
        Me.cmdOpenFileDialog.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdOpenFileDialog.Location = New System.Drawing.Point(285, 40)
        Me.cmdOpenFileDialog.Name = "cmdOpenFileDialog"
        Me.cmdOpenFileDialog.Size = New System.Drawing.Size(29, 25)
        Me.cmdOpenFileDialog.TabIndex = 26
        Me.cmdOpenFileDialog.Text = "..."
        Me.cmdOpenFileDialog.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.cmdOpenFileDialog.UseVisualStyleBackColor = True
        Me.cmdOpenFileDialog.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdoSWTOR)
        Me.GroupBox1.Controls.Add(Me.rdoDeathstalker)
        Me.GroupBox1.Controls.Add(Me.rdoBlade)
        Me.GroupBox1.Location = New System.Drawing.Point(341, 49)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(164, 111)
        Me.GroupBox1.TabIndex = 29
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Devices"
        '
        'rdoSWTOR
        '
        Me.rdoSWTOR.AutoSize = True
        Me.rdoSWTOR.Location = New System.Drawing.Point(22, 66)
        Me.rdoSWTOR.Name = "rdoSWTOR"
        Me.rdoSWTOR.Size = New System.Drawing.Size(66, 17)
        Me.rdoSWTOR.TabIndex = 2
        Me.rdoSWTOR.Text = "SWTOR"
        Me.rdoSWTOR.UseVisualStyleBackColor = True
        '
        'rdoDeathstalker
        '
        Me.rdoDeathstalker.AutoSize = True
        Me.rdoDeathstalker.Checked = True
        Me.rdoDeathstalker.Location = New System.Drawing.Point(22, 43)
        Me.rdoDeathstalker.Name = "rdoDeathstalker"
        Me.rdoDeathstalker.Size = New System.Drawing.Size(126, 17)
        Me.rdoDeathstalker.TabIndex = 1
        Me.rdoDeathstalker.TabStop = True
        Me.rdoDeathstalker.Text = "Deathstalker Ultimate"
        Me.rdoDeathstalker.UseVisualStyleBackColor = True
        '
        'rdoBlade
        '
        Me.rdoBlade.AutoSize = True
        Me.rdoBlade.Location = New System.Drawing.Point(22, 20)
        Me.rdoBlade.Name = "rdoBlade"
        Me.rdoBlade.Size = New System.Drawing.Size(83, 17)
        Me.rdoBlade.TabIndex = 0
        Me.rdoBlade.Text = "Razer Blade"
        Me.rdoBlade.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Enabled = False
        Me.Label4.Location = New System.Drawing.Point(13, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 13)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "Current Directory"
        Me.Label4.Visible = False
        '
        'cmdGetAppList
        '
        Me.cmdGetAppList.Location = New System.Drawing.Point(358, 166)
        Me.cmdGetAppList.Name = "cmdGetAppList"
        Me.cmdGetAppList.Size = New System.Drawing.Size(131, 43)
        Me.cmdGetAppList.TabIndex = 31
        Me.cmdGetAppList.Text = "Get Device Applist"
        Me.cmdGetAppList.UseVisualStyleBackColor = True
        '
        'cmdApply
        '
        Me.cmdApply.Enabled = False
        Me.cmdApply.Location = New System.Drawing.Point(762, 375)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 32
        Me.cmdApply.TabStop = False
        Me.cmdApply.Text = "&Apply"
        Me.cmdApply.UseVisualStyleBackColor = True
        Me.cmdApply.Visible = False
        '
        'cmdAbout
        '
        Me.cmdAbout.Location = New System.Drawing.Point(695, 12)
        Me.cmdAbout.Name = "cmdAbout"
        Me.cmdAbout.Size = New System.Drawing.Size(75, 23)
        Me.cmdAbout.TabIndex = 33
        Me.cmdAbout.Text = "&About"
        Me.cmdAbout.UseVisualStyleBackColor = True
        '
        'frmRazerQuickSwitch
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(782, 368)
        Me.Controls.Add(Me.cmdAbout)
        Me.Controls.Add(Me.cmdApply)
        Me.Controls.Add(Me.cmdGetAppList)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdOpenFileDialog)
        Me.Controls.Add(Me.txtNewOrder)
        Me.Controls.Add(Me.txtCurrentOrder)
        Me.Controls.Add(Me.cmdMoveDown)
        Me.Controls.Add(Me.cmdMoveUp)
        Me.Controls.Add(Me.cmdMoveFirst)
        Me.Controls.Add(Me.cmdMoveLast)
        Me.Controls.Add(Me.cmdOk)
        Me.Controls.Add(Me.cmdOrderApply)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.label3)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.lstInput)
        Me.Name = "frmRazerQuickSwitch"
        Me.Text = "Razer Quick Switch - made by Masamune_Shadow"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents txtNewOrder As System.Windows.Forms.TextBox
    Private WithEvents txtCurrentOrder As System.Windows.Forms.TextBox
    Private WithEvents cmdMoveDown As System.Windows.Forms.Button
    Private WithEvents cmdMoveUp As System.Windows.Forms.Button
    Private WithEvents cmdMoveFirst As System.Windows.Forms.Button
    Private WithEvents cmdMoveLast As System.Windows.Forms.Button
    Private WithEvents cmdOk As System.Windows.Forms.Button
    Private WithEvents cmdOrderApply As System.Windows.Forms.Button
    Private WithEvents cmdCancel As System.Windows.Forms.Button
    Private WithEvents label3 As System.Windows.Forms.Label
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents label1 As System.Windows.Forms.Label
    Private WithEvents lstInput As System.Windows.Forms.ListBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Private WithEvents cmdOpenFileDialog As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rdoSWTOR As System.Windows.Forms.RadioButton
    Friend WithEvents rdoDeathstalker As System.Windows.Forms.RadioButton
    Friend WithEvents rdoBlade As System.Windows.Forms.RadioButton
    Private WithEvents Label4 As System.Windows.Forms.Label
    Private WithEvents cmdGetAppList As System.Windows.Forms.Button
    Private WithEvents cmdApply As System.Windows.Forms.Button
    Private WithEvents cmdAbout As System.Windows.Forms.Button

End Class
