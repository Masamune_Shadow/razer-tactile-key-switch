﻿Option Explicit On
Imports System
Imports System.IO


Public Class frmRazerQuickSwitch
    Public szInput As String = ""
    Public fUseDevice As Integer
    Public azListArray As New ArrayList
    Public azRawListArray(30) As String
    Public selectedOrder As Integer
    Public selectedString As String
    '1 = Blade
    '2 = Deathstalker
    '3 = SWTOR
    '4 = All

    'Public fDeathstalker As Boolean
    'Public fDeathstalker As Boolean
    'Public fSWTOR As Boolean
    'Public fAll As Boolean

    Private Sub frmRazerQuickSwitch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'OpenFileDialog1
    End Sub

    Private Sub cmdOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOk.Click
        Commit()
        End
    End Sub
    Private Sub Commit()
        Dim rawName, OutputString, szNameToUse As String

        OutputString = "[Apps]" & vbCrLf
        For i = 0 To azListArray.Count - 1
            szNameToUse = ParseList(lstInput.Items(i), False)
            rawName = GetTranslatedName(szNameToUse, False)
            OutputString += ((CStr(i) + "=" + rawName) & vbCrLf)
        Next
        'eof
        'file close
        If OutputString <> ("[Apps]" & vbCrLf) Then
            Using outfile As New StreamWriter(szInput)
                outfile.Write(OutputString)
            End Using
        End If
    End Sub
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        End
    End Sub
    Private Sub cmdApply_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        Commit()
        Refresh()
    End Sub

    Private Sub Refresh()
        cmdGetAppList.PerformClick()
    End Sub
    Private Function GetTranslatedName(ByVal inputString As String, ByVal isTranslating As Boolean) As String
        'isTranslating = true = translate
        'isTranslating = false = untranslate
        If isTranslating Then
            Select Case inputString
                Case "320F7C2F-C8C2-42E8-9CC5-B6823DA3B22C"
                    Return "TRACKPAD"
                Case "91845076-CD47-435F-A442-CEB373A3ADE8"
                    Return "NUMPAD"
                Case "D8D62D6D-5187-441A-88E9-2A035EF63237"
                    Return "MACRO RECORDER"
                Case "89F31BC8-E2B5-4843-B208-A2A24AA9A902"
                    Return "GAME MODE"
                Case "A5716312-5C62-4E0C-8BEF-A7B531049095"
                    Return "BROWSER"
                Case "7ADAA9C4-0E77-4E9E-B071-4CD27C41D1F7"
                    Return "YOUTUBE"
                Case "12702956-BFBD-440C-8185-9671D1F78E9C"
                    Return "FACEBOOK"
                Case "E79E002F-A564-4A1C-9411-38DD4EFCE5EE"
                    Return "TWITTER"
                Case "8989837D-7ED6-47E7-82C0-752123AF2EA7"
                    Return "GMAIL"
                Case "945749A0-B4C2-4EB5-A93E-44DC10FDAF4D"
                    Return "CLOCK"
                Case "977879DF-8520-49C7-871C-EAA0311F1CE1"
                    Return "COUNTER STRIKE"
                Case "D2541ECA-DAAD-4182-9AF6-1FEAE474F88C"
                    Return "TF2"
                Case "45EEDEDE-60B4-43FF-BAF7-5047DD85D2C4"
                    Return "BF3"
                Case "A0419FBF-F66D-4740-A525-DB9486DA5556"
                    Return "TIMER"
                Case "65BFE244-2354-4E41-ADC9-CCF6BE3B5F75"
                    Return "SCREENSHOT"
                Case "76B1EEFA-8F21-42A0-86A7-C2A30C94FCB7"
                    Return "SWTOR COMBAT LOGGER"
                Case "69C3A98B-186D-43AB-B7DC-695AB25ECB50"
                    Return "SKYRIM"
                Case "34BD2A95-1164-42FB-854F-42913A378E07"
                    Return "LEAGUE OF LEGENDS"
                Case "337C6D87-1D1F-45C7-B5D1-226FAD2B2739"
                    Return "Media Player"
                Case "1"
                    Return "Masamune_Shadow's Numpad"
                Case "2"
                    Return "MichaelMc's VLC Streamer"
                Case Else
                    Return inputString
                    'Case ""
                    '   Return "this program"
                    'Case ""
                    'Return "VLC streamer"
                    'Case "1"
                    'Return "MASAMUNE_SHADOW NUMPAD"
            End Select
        Else
            Select Case inputString
                Case "TRACKPAD"
                    Return "320F7C2F-C8C2-42E8-9CC5-B6823DA3B22C"
                Case "NUMPAD"
                    Return "91845076-CD47-435F-A442-CEB373A3ADE8"
                Case "MACRO RECORDER"
                    Return "D8D62D6D-5187-441A-88E9-2A035EF63237"
                Case "GAME MODE"
                    Return "89F31BC8-E2B5-4843-B208-A2A24AA9A902"
                Case "BROWSER"
                    Return "A5716312-5C62-4E0C-8BEF-A7B531049095"
                Case "YOUTUBE"
                    Return "7ADAA9C4-0E77-4E9E-B071-4CD27C41D1F7"
                Case "FACEBOOK"
                    Return "12702956-BFBD-440C-8185-9671D1F78E9C"
                Case "TWITTER"
                    Return "E79E002F-A564-4A1C-9411-38DD4EFCE5EE"
                Case "GMAIL"
                    Return "8989837D-7ED6-47E7-82C0-752123AF2EA7"
                Case "CLOCK"
                    Return "945749A0-B4C2-4EB5-A93E-44DC10FDAF4D"
                Case "COUNTER STRIKE"
                    Return "977879DF-8520-49C7-871C-EAA0311F1CE1"
                Case "TF2"
                    Return "D2541ECA-DAAD-4182-9AF6-1FEAE474F88C"
                Case "BF3"
                    Return "45EEDEDE-60B4-43FF-BAF7-5047DD85D2C4"
                Case "TIMER"
                    Return "A0419FBF-F66D-4740-A525-DB9486DA5556"
                Case "SCREENSHOT"
                    Return "65BFE244-2354-4E41-ADC9-CCF6BE3B5F75"
                Case "SWTOR COMBAT LOGGER"
                    Return "76B1EEFA-8F21-42A0-86A7-C2A30C94FCB7"
                Case "SKYRIM"
                    Return "69C3A98B-186D-43AB-B7DC-695AB25ECB50"
                Case "LEAGUE OF LEGENDS"
                    Return "34BD2A95-1164-42FB-854F-42913A378E07"
                Case "Media Player"
                    Return "337C6D87-1D1F-45C7-B5D1-226FAD2B2739"
                Case "Masamune_Shadow's Numpad"
                    Return "1"
                Case "MichaelMc's VLC Streamer"
                    Return "2"
                Case Else
                    Return inputString
                    'Case ""
                    '   Return "this program"
                    'Case ""
                    'Return "VLC streamer"
                    'Case "1"
                    'Return "MASAMUNE_SHADOW NUMPAD"


            End Select
        End If
    End Function
    Private Sub ParseFileName(ByVal line As String, ByRef order As Integer, ByRef filename As String)
        If line <> "[Apps]" Then
            Dim parsed() As String = line.Split(New Char() {"="c})
            order = CInt(parsed(0))
            filename = parsed(1)
        End If
        'get the order from the file name
        'order = CInt()
        'filename = remaining

    End Sub
    Private Function ParseList(ByVal selectedString As String, ByVal wantOrder As Boolean) As String
        'prase out the order
        Dim seperator() As String = {"-", " "}
        Dim formattedString As String
        Dim parsed() As String = selectedString.Split(seperator, StringSplitOptions.RemoveEmptyEntries)
        If (wantOrder) Then
            formattedString = parsed(0)
        Else
            Dim i As Integer = 0
            For Each word As String In parsed
                If i > 0 Then 'ignore the first value in parsed (the order #)
                    If i > 1 Then
                        formattedString += (" " + word)
                    Else
                        formattedString = word
                    End If
                End If
                i += 1
            Next

        End If
        Return formattedString
    End Function
    Private Sub ParseFileList()
        Dim order As Integer
        Dim filename As String
        Dim translatedname As String
        Try
            ' Create an instance of StreamReader to read from a file.
            ' The using statement also closes the StreamReader.
            Using sr As StreamReader = File.OpenText(szInput)
                Dim line As String
                ' Read and display lines from the file until the end of
                ' the file is reached.
                Do
                    line = sr.ReadLine()
                    If Not (line Is Nothing) Then
                        ParseFileName(line, order, filename)
                        If (filename <> String.Empty) Then
                            translatedname = GetTranslatedName(filename, True)
                            azListArray.Insert(order, translatedname)
                        End If
                    End If
                Loop Until line Is Nothing
                FillList()
            End Using
        Catch e As Exception
            ' Let the user know what went wrong.
            Console.WriteLine("The file could not be read:")
            Console.WriteLine(e.Message)
        End Try
    End Sub
    Private Sub FillList()
        Dim output As String
        For i = 0 To azListArray.Count - 1
            output = (CStr(i) + " - " + azListArray(i).ToString())
            lstInput.Items.Add(output)
        Next
    End Sub
    Private Sub GetDevice()
        If (rdoBlade.Checked) Then
            fUseDevice = 1
        ElseIf rdoDeathstalker.Checked Then
            fUseDevice = 2
        ElseIf rdoSWTOR.Checked Then
            fUseDevice = 3
        End If
    End Sub
    Private Sub cmdGetAppList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGetAppList.Click
        lstInput.Items.Clear()
        azListArray.Clear()
        GetDevice()
        If fUseDevice <> 0 Then
            If fUseDevice = 1 Then
                szInput = "C:\ProgramData\Razer\SwitchBlade\Blade\Razer\1068AAE3-6299-4086-A7F6-0600F5F1D1E5\Apps.ini"
            ElseIf fUseDevice = 2 Then
                szInput = "C:\ProgramData\Razer\SwitchBlade\DeathStalker\Razer\1068AAE3-6299-4086-A7F6-0600F5F1D1E5\Apps.ini"
            ElseIf fUseDevice = 3 Then
                szInput = "C:\ProgramData\Razer\SwitchBlade\SWTOR\Razer\1068AAE3-6299-4086-A7F6-0600F5F1D1E5\Apps.ini"
            End If
            ParseFileList()
        End If
    End Sub

    Private Sub cmdMoveUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMoveUp.Click
        If selectedOrder <> 0 Then
            Dim oldOrder, newOrder As Integer
            Dim oldString, newString As String
            oldOrder = selectedOrder
            oldString = selectedString

            newOrder = selectedOrder - 1
            newString = ParseList(lstInput.Items(newOrder), False)
            'azListArray(newOrder)

            lstInput.Items(newOrder) = FormatOutput(newOrder, oldString, True)
            lstInput.Items(oldOrder) = FormatOutput(oldOrder, newString, True)
            lstInput.SelectedIndex = newOrder
        End If
    End Sub

    Private Sub cmdMoveFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMoveFirst.Click
        'make the object @ azListArray(currentorder) become azListArray.nextOrder

        Dim oldOrder, newOrder As Integer
        Dim oldString, newString As String
        oldOrder = selectedOrder
        oldString = selectedString

        'newOrder = selectedOrder - 1
        newString = lstInput.Items(0)

        lstInput.Items(0) = FormatOutput(0, oldString, True)
        lstInput.Items(oldOrder) = FormatOutput(oldOrder, newString, True)

    End Sub

    Private Sub cmdMoveDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMoveDown.Click
        If selectedOrder <> azListArray.Count - 1 Then
            Dim oldOrder, newOrder As Integer
            Dim oldString, newString As String
            oldOrder = selectedOrder
            oldString = selectedString

            newOrder = selectedOrder + 1
            newString = ParseList(lstInput.Items(newOrder), False)

            lstInput.Items(newOrder) = FormatOutput(newOrder, oldString, True)
            lstInput.Items(oldOrder) = FormatOutput(oldOrder, newString, True)
            lstInput.SelectedIndex = newOrder
        End If
    End Sub

    Private Sub cmdMoveLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMoveLast.Click

    End Sub

    Private Sub lstInput_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstInput.SelectedIndexChanged
        If lstInput.SelectedItem <> Nothing Then
            txtCurrentOrder.Text = ParseList(lstInput.SelectedItem.ToString(), True)
            selectedOrder = CInt(txtCurrentOrder.Text)
            selectedString = ParseList(lstInput.Items(selectedOrder), False)
        End If
    End Sub
    Private Sub cmdOrderApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOrderApply.Click
        'UpdateList() 'updates the list with the new order
    End Sub
    Private Function FormatOutput(ByVal order As Integer, ByVal name As String, ByVal isTranslated As Boolean) As String
        If (isTranslated) Then
            Return (CStr(order) + " - " + name)
        Else
            Return (CStr(order) + "=" + name)
        End If

    End Function

    Private Sub cmdAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAbout.Click
        Dim szAbout As String
        szAbout = "Created By: Spencer Johnson (Masamune_Shadow) 10/28/2012)" & vbCrLf
        szAbout = "Updated By: Spencer Johnson (Masamune_Shadow) 2/28/2013)" & vbCrLf
        szAbout += "Email: masamune.shadow@gmail.com" & vbCrLf & vbCrLf
        szAbout += "To Use: Pick your device, swap the order, hit OK, and then restart the razer synapse (if you have a better way to get it to show up, please let me know)" & vbCrLf

        MsgBox(szAbout, vbOKOnly, "About")
    End Sub
End Class
